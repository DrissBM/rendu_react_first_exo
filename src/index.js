// index.js
import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import App from './components/App';

const ReactRoot = document.getElementById('root');
axios
  .get('https://randomuser.me/api/')
  .then(response => {
    const {data} = response;
    ReactDOM.render(<App data={data} />, ReactRoot);
  })
  .catch(error => console.error(error));

module.hot.accept();
