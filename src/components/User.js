import React from 'react';
// import PropTypes from 'prop-types';

const User = ({
  img,
  firstname = 'yo',
  lastname = 'rien ne change',
  username,
  age,
  mail,
  phone,
}) => {
  console.log('yoyo');
  function handleClick(e) {
    e.currentTarget.classList.toggle('stroke');
  }
  return (
    <div className="user">
      <img src={img} />
      <p className="name">
        {firstname} {lastname}
      </p>
      <div className="info">
        <p>
          Nom d'utilisateur: <span onClick={handleClick}>{username}</span>
        </p>
        <p>
          Age: <span onClick={handleClick}>{age}</span>
        </p>
        <p>
          Email: <span onClick={handleClick}>{mail}</span>
        </p>
        <p>
          Tél: <span onClick={handleClick}>{phone}</span>
        </p>
      </div>
    </div>
  );
};

export default User;
