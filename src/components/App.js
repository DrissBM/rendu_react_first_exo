import React from 'react';
import ListUsers from './ListUsers';
import '../style.scss';

const App = props => {
  return (
    <div>
      <ListUsers list={props.data.results} />
    </div>
  );
};

export default App;
