import React from 'react';
import PropTypes from 'prop-types';

const Post = ({title = 'yo', content = 'rien ne change'}) => {
  const handleClick = () => {
    console.log('Vous avez cliquez sur la liste');
  };
  const handleMouseOver = event => {
    console.log(`Vous survolez${event.target.className}`);
  };
  return (
    <div className="post" onClick={handleClick}>
      <h3 className="post__title" onMouseOver={handleMouseOver}>
        {title}
      </h3>
      {content}
    </div>
  );
};

Post.defaultProps = {
  title: 'Aucun titre défini',
  content: 'Pas de contenu défini',
};

Post.propTypes = {
  title: PropTypes.string,
  content: PropTypes.string.isRequired,
};

export default Post;
