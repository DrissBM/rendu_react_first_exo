import React from 'react';
import User from './User';
// import PropTypes from 'prop-types';

const ListUsers = ({list}) => {
  return (
    <div className="list">
      {list.map((element, index) => (
        <User
          key={index}
          img={element.picture.large}
          firstname={element.name.first}
          lastname={element.name.last}
          username={element.login.username}
          age={element.dob.age}
          mail={element.email}
          phone={element.cell}
        />
      ))}
    </div>
  );
};

export default ListUsers;
