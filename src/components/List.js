import React from 'react';
import Post from './Post';
import PropTypes from 'prop-types';

const List = ({posts}) => {
  return (
    <div className="list">
      {posts.map((element, index) => (
        <Post key={index} {...element} />
      ))}
    </div>
  );
};

List.defaultProps = {
  posts: [
    {
      title: 'Ma list est vide',
      content: 'gérer ma list',
    },
  ],
};

List.propTypes = {
  posts: PropTypes.arrayOf(Object),
};

export default List;
